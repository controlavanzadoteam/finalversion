/*
 * control.h
 *
 *  Created on: Sep 9, 2016
 *      Author: oscar
 */

#ifndef NONLINEAL_H_
#define NONLINEAL_H_

#include <stdbool.h>
#include <stdint.h>
#include "stabilizer_types.h"

bool vNonLinealTest(void);
void vNonLinealControl(const setpoint_t *setpoint,const sensorData_t *sensorData,state_t *state);
void vNonLinealControl_init(void);
#endif /* SRC_MODULES_INTERFACE_CONTROL_H_ */
	