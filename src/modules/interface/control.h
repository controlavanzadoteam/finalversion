/*
 * control.h
 *
 *  Created on: Sep 9, 2016
 *      Author: oscar
 */

#ifndef SRC_MODULES_INTERFACE_CONTROL_H_
#define SRC_MODULES_INTERFACE_CONTROL_H_

#include <stdbool.h>
#include <stdint.h>

void controlInit(void);
bool controlTest(void);


#endif /* SRC_MODULES_INTERFACE_CONTROL_H_ */
